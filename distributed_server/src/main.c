#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>

#include "../inc/global.h"
#include "../inc/gpio.h"
#include "../inc/i2c.h"
#include "../inc/servidor_tcp.h"
#include "../inc/interrupcao.h"

void catch_ctrl_c_and_exit(int);
void init_sys();

int main(int argc, char **argv){

  if(argc == 2)
    if (argv[1] == 'T' || argv[1] == '1'){
      andar = argv[1];
      PORT = argv[1] == 'T' ? TERREO_PORT : ANDAR_PORT;
    }

	signal(SIGINT, catch_ctrl_c_and_exit);

  init_sys();

  init_client();

  return 0;
}

void init_sys(){
  if(wiringPiSetup() == -1){
    printf("Cannot setup wiringPi.\n");
    exit(1);
  }

  init_gpio();
  init_sen_state();
  set_interrupt();
}

void catch_ctrl_c_and_exit(int sig) {
  char mensagem_finalizacao[] = "FIM";
  select(sock_cli, &fds, NULL, NULL, &tv);
  send(sock_cli, &mensagem_finalizacao, 3*sizeof(char), 0);
  desliga_aparelhos();
  close(sock_cli);
  exit(0);
}
