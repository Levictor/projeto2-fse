#include"../inc/gpio.h"

struct timeval last_changed;

void init_gpio(){
// TERREO
  if(andar == 'T'){
    pinMode(LS_T01, OUTPUT);
    pinMode(LS_T02, OUTPUT);
    pinMode(LC_T, OUTPUT);
    pinMode(AC_T, OUTPUT);
    pinMode(ASP, OUTPUT);

    pinMode(SP_T, INPUT);
    pinMode(SF_T, INPUT);
    pinMode(SJ_T01, INPUT);
    pinMode(SJ_T02, INPUT);
    pinMode(SPo_T, INPUT);
    pinMode(SC_OUT, INPUT);
    pinMode(SC_IN, INPUT);
  }
// PRIMEIRO ANDAR
  else {
    pinMode(LS_101, OUTPUT);
    pinMode(LS_102, OUTPUT);
    pinMode(LC_1, OUTPUT);
    pinMode(AC_1, OUTPUT);

    pinMode(SP_1, INPUT);
    pinMode(SF_1, INPUT);
    pinMode(SJ_101, INPUT);
    pinMode(SJ_102, INPUT);
  }
  ler_estados();
}

void turn_on(int addr){
  digitalWrite(addr, HIGH);
}

void turn_off(int addr){
  digitalWrite(addr, LOW);
}

int change_state(int addr){
  int r = 0;
  if(digitalRead(addr)){
    turn_off(addr);
  }
  else{
    turn_on(addr);
    r = 1;
  }
  return r;
}

void ler_estados(){
  if(andar == 'T'){
    sensor_t.presenca = digitalRead(LS_T01);
    sensor_t.fumaca = digitalRead(LS_T02);
    sensor_t.janela1 = digitalRead(LC_T);
    sensor_t.janela2 = digitalRead(AC_T);
    sensor_t.entrada = digitalRead(ASP);
    sensor_t.entrada_predio = digitalRead(ASP);
    sensor_t.saida_predio = digitalRead(ASP);
  }
  else{
    t[0] = digitalRead(LS_101);
    t[1] = digitalRead(LS_102);
    t[2] = digitalRead(LC_1);
    t[3] = digitalRead(AC_1);
  }
}

void desliga_aparelhos(){
// TERREO
  if(andar == 'T'){
    digitalWrite(LS_T01, LOW);
    digitalWrite(LS_T02, LOW);
    digitalWrite(LC_T, LOW);
    digitalWrite(AC_T, LOW);
    digitalWrite(ASP, LOW);
  }
// PRIMEIRO ANDAR
  else {
    digitalWrite(LS_101, LOW);
    digitalWrite(LS_102, LOW);
    digitalWrite(LC_1, LOW);
    digitalWrite(AC_1, LOW);
  }
}

void init_sen_state(){

  gettimeofday(&last_changed, NULL);
  int temp, hum;

  if(andar == 'T'){
    lc_sp_t = last_changed;
    lc_sf_t = last_changed;
    lc_sc_in = last_changed;
    lc_sj_1_t = last_changed;
    lc_sj_2_t = last_changed;
    lc_sc_out = last_changed;
    lc_spo_t = last_changed;

    sensores_t.presenca = digitalRead(SP_T);
    sensores_t.fumaca = digitalRead(SF_T);
    sensores_t.janela1 = digitalRead(SJ_T01);
    sensores_t.janela2 = digitalRead(SJ_T02);
    sensores_t.entrada = digitalRead(SPo_T);
    sensores_t.saida_predio = digitalRead(SC_OUT);
    sensores_t.entrada_predio = digitalRead(SC_IN);
  }
  else{
    lc_sp_1 = last_changed;
    lc_sf_1 = last_changed;
    lc_sj_1_1 = last_changed;
    lc_sj_2_1 = last_changed;

    sensores_a.presenca = digitalRead(SP_1);
    sensores_a.fumaca = digitalRead(SF_1);
    sensores_a.janela1 = digitalRead(SJ_101);
    sensores_a.janela2 = digitalRead(SJ_102);
  }
  do{
    temp = read_dht_data(TEMP_1, 'T');
    hum = read_dht_data(TEMP_1, 'H');
  }
  while(temp > 0 && hum > 0);
  temperatura_humidade.temperatura = temp;
  temperatura_humidade.humidade = hum;
  desliga_aparelhos();
}
