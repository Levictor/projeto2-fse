#include "../inc/interrupcao.h"

void verifica_sensor(int* s, int addr){
  *s = digitalRead(addr);
  send_sns(addr, *s);
  if(*s){
    printf("Rising - %d\n", addr);
  }
  else{
    printf("Falling - %d\n", addr);
  }
}

unsigned long get_diff(struct timeval*lc){
  struct timeval now;
  unsigned long diff;

  gettimeofday(&now, NULL);

  diff = (now.tv_sec *1000000 + now.tv_usec) - (lc->tv_sec * 1000000 + lc->tv_usec);

  *lc = now;

  return diff;
}

void trata_sinal_presenca_t(){
  if(get_diff(&lc_sp_t) > IGNORE_TIME){
    send_sns(SP_T, digitalRead(SP_T));
  }
}

void trata_sinal_fumaca_t(){
  if(get_diff(&lc_sf_t) > IGNORE_TIME){
    send_sns(SF_T, digitalRead(SF_T));
  }
}

void trata_sinal_janela_1_t(){
  if(get_diff(&lc_sj_1_t) > IGNORE_TIME){
    send_sns(SJ_T01, digitalRead(SJ_101));
  }
}

void trata_sinal_janela_2_t(){
  if(get_diff(&lc_sj_2_t) > IGNORE_TIME){
    send_sns(SJ_T02, digitalRead(SJ_T02));
  }
}

void trata_sinal_entrada(){
  if(get_diff(&lc_spo_t) > IGNORE_TIME){
    send_sns(SPo_T, digitalRead(SPo_T));
  }
}

void trata_sinal_catraca_e(){
  if(get_diff(&lc_sc_in) > IGNORE_TIME){
    send_sns(SC_IN, digitalRead(SC_IN));
  }
}

void trata_sinal_catraca_s(){
  if(get_diff(&lc_sc_out) > IGNORE_TIME){
    send_sns(SC_OUT, digitalRead(SC_OUT));
  }
}

void trata_sinal_temp_hum_t(){
  int temp, hum;
  temp = read_dht_data(TEMP_T, 'T');
  hum = read_dht_data(TEMP_T, 'H');
  if(temp > 0 && hum > 0){
    temperatura_humidade.temperatura = temp;
    temperatura_humidade.humidade = hum;
  }
}

void trata_sinal_presenca_1(){
  if(get_diff(&lc_sp_1) > IGNORE_TIME){
    send_sns(SP_1, digitalRead(SP_1));
  }
}

void trata_sinal_fumaca_1(){
  if(get_diff(&lc_sf_1) > IGNORE_TIME){
    send_sns(SF_1, digitalRead(SF_1));
  }
}

void trata_sinal_janela_1_1(){
  if(get_diff(&lc_sj_1_1) > IGNORE_TIME){
    send_sns(SJ_101, digitalRead(SJ_101));
  }
}

void trata_sinal_janela_2_1(){
  if(get_diff(&lc_sj_2_1) > IGNORE_TIME){
    send_sns(SJ_102, digitalRead(SJ_102));
  }
}

void trata_sinal_temp_hum_1(){
  int temp, hum;
  temp = read_dht_data(TEMP_1, 'T');
  hum = read_dht_data(TEMP_1, 'H');
  if(temp > 0 && hum > 0){
    temperatura_humidade.temperatura = temp;
    temperatura_humidade.humidade = hum;
  }
}

void set_interrupt(){
  if(andar == 'T'){
    wiringPiISR(SP_T, INT_EDGE_BOTH, trata_sinal_presenca_t);
    wiringPiISR(SF_T, INT_EDGE_BOTH, trata_sinal_fumaca_t);
    wiringPiISR(SJ_T01, INT_EDGE_BOTH, trata_sinal_janela_1_t);
    wiringPiISR(SJ_T02, INT_EDGE_BOTH, trata_sinal_janela_2_t);
    wiringPiISR(SPo_T, INT_EDGE_BOTH, trata_sinal_entrada);
    wiringPiISR(SC_OUT, INT_EDGE_BOTH, trata_sinal_catraca_e);
    wiringPiISR(SC_IN, INT_EDGE_BOTH, trata_sinal_catraca_s);
    wiringPiISR(TEMP_T, INT_EDGE_BOTH, trata_sinal_osen6);
  }

  else{
    wiringPiISR(SP_1, INT_EDGE_BOTH, trata_sinal_presenca_1);
    wiringPiISR(SF_1, INT_EDGE_BOTH, trata_sinal_fumaca_1);
    wiringPiISR(SJ_101, INT_EDGE_BOTH, trata_sinal_janela_1_1);
    wiringPiISR(SJ_102, INT_EDGE_BOTH, trata_sinal_janela_2_1);
    wiringPiISR(TEMP_1, INT_EDGE_BOTH, trata_sinal_osen3);
  }
}
