#include "../inc/cliente_tcp.h"

void send_sns(int sensor, int valor){
  FD_ZERO(&fds);
  FD_SET(0, &fds);
  
  mensagem msg;
  msg.sensor = sensor;
  msg.valor = valor;

  tv.tv_sec = 3;
  if(sock_cli > 0){
    select(sock_cli, &fds, NULL, NULL, &tv);
    if(send(sock_cli, &msg, sizeof(mensagem), 0) != sizeof(mensagem))
      printf("Erro no envio: numero de bytes enviados diferente do esperado\n");
  }
}

int init_client() {
	struct timeval tv;

  FD_ZERO(&fds);
	FD_SET(0, &fds);
	tv.tv_sec = 1;
	tv.tv_usec = 1;


  SADDRIN end_serv;
  sock_cli = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(sock_cli < 0)
		printf("Erro no socket()\n");

	// Construir SADDRIN
	memset(&end_serv, 0, sizeof(end_serv)); // Zerando a estrutura de dados
	end_serv.sin_family = AF_INET;
	end_serv.sin_addr.s_addr = inet_addr(SERVER_IP);
	end_serv.sin_port = htons(PORT);

	printf("port - %d\nIP - %s\n", PORT,SERVER_IP);

	// Connect
	if(connect(sock_cli, (SADDR *) &end_serv, sizeof(end_serv)) < 0){
		printf("Erro no connect()\n");
    return 0;
  }
  select(sock_cli, &fds, NULL, NULL, &tv);

	send(sock_cli, &andar, 1, 0);

  comunicacao_servidor();

  return 1;
}

void comunicacao_servidor(){
  int tam_msg;
  mensagem msg;
  while(1){
    tam_msg = recv(sock_cli, &msg, sizeof(mensagem), 0);
    if(*msg.sensor == -1){
      select(sock_cli, &fds, NULL, NULL, &tv);
      send(sock_cli, &temperatura_humidade, sizeof(temp_hum), 0);
    }
    else if (msg.sensor > 0 && msg.valor > 0){
      if(msg.valor)
        turn_on(msg.sensor);
      else
        turn_off(msg.sensor);
    }
  }
}
