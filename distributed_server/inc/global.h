#ifndef GLOBAL_H
#define GLOBAL_H

#include <sys/time.h>
#include "../../utils/socket_constant.h"

extern temp_hum temperatura_humidade;

extern aparelhos_terreo aparelhos_t;

extern aparelhos_andar aparelhos_a;

extern sensores_terreo sensores_t;

extern sensores_andar sensores_a;

extern int PORT; 

extern char andar;
           
extern struct timeval lc_sp_t, lc_sf_t, lc_sj_1_t, lc_sc_in, lc_sj_2_t, lc_sc_out, lc_spo_t;
extern struct timeval lc_sp_1, lc_sf_1, lc_sj_1_1, lc_sj_2_1;

void set_sensores_terreo(sensores_terreo);

void set_sensores_andar(sensores_andar);

#endif
