#ifndef _INTERRUPCAO_H
#define _INTERRUPCAO_H

#include <sys/socket.h>
#include <wiringPi.h>
#include <stdio.h>

#include "./global.h"
#include "./cliente_tcp.h"

#define IGNORE_TIME 10000

void verifica_sensor(int*,int);
unsigned long get_diff(struct timeval*);

void trata_sinal_psen1();
void trata_sinal_psen2();
void trata_sinal_osen1();
void trata_sinal_osen2();
void trata_sinal_osen3();
void trata_sinal_osen4();
void trata_sinal_osen5();
void trata_sinal_osen6();

void set_interrupt();



#endif
