#ifndef DHT22_H
#define DHT22_H


#include "./global.h"
#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

// CONSTANTS 
#define MAX_TIMINGS	85
#define DEBUG 0
#define WAIT_TIME 2000
#define DHT_PIN_T 20
#define DHT_PIN_1 21


int data[5] = { 0, 0, 0, 0, 0 };

int leitura_dht(int, char);
