#ifndef _SERVIDOR_TCP_H
#define _SERVIDOR_TCP_H


#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../../utils/socket_constant.h"
#include "../inc/gpio.h"
#include "../inc/i2c.h"
#define SADDR struct sockaddr
#define SADDRIN struct sockaddr_in


int comunicacao_servidor(int);
void socket_server();


#endif
