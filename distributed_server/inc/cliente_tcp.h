#ifndef _CLIENTE_TCP_H
#define _CLIENTE_TCP_H

#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../inc/global.h"
#include "./gpio.h"
#include "../../utils/socket_constant.h"

#define SERVER_IP "192.168.0.53"

#define SADDR struct sockaddr
#define SADDRIN struct sockaddr_in

int sock_cli;
SADDRIN end_serv;
fd_set fds;
struct timeval tv;

int check_sns();
void send_sns(int, int);
int init_client();
void comunicacao_servidor();

#endif
