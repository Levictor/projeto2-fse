#ifndef _CONSTANT_H
#define _CONSTANT_H

#define IP_CENTRAL 192.168.0.53
#define IP_DISRTRIBUIDO 192.168.0.52

// ENDERECOS SAIDA TERREO
#define LS_T01 7
#define LS_T02 0
#define LC_T 2
#define AC_T 11
#define ASP 27

// ENDERECOS ENTRADA TERREO
#define SP_T 25
#define SF_T 4 
#define SJ_T01 13
#define SJ_T02 14
#define SPo_T 12
#define SC_OUT 24
#define SC_IN 23
#define TEMP_T 28

// ENDERECOS SAIDA ANDARES
#define LS_101 3
#define LS_102 6
#define LC_1 10
#define AC_1 26

// ENDERECOS ENTRADA ANDARES
#define SP_1 1
#define SF_1 5 
#define SJ_101 21
#define SJ_102 22
#define TEMP_1 29

#define CENTRAL_PORT 10030
#define TERREO_PORT 10130
#define ANDAR_PORT 10230

typedef struct mensagem{
  int sensor, valor;
}mensagem;

typedef struct temp_hum{
  float temperatura, humidade;
}temp_hum;

typedef struct aparelhos_andar{
  int luz_1, luz_2,
    luz_c, ar;
}aparelhos_andar;

typedef struct aparelhos_terreo{
  int luz_1, luz_2,
    luz_c, ar,
    asp, alarme;
}aparelhos_terreo;

typedef struct sensores_terreo{
  int presenca, fumaca,
    janela1, janela2,
    entrada, entrada_predio,
    saida_predio;
}sensores_terreo;

typedef struct sensores_andar{
  int presenca, fumaca,
    janela1, janela2;
}sensores_andar;

#endif
