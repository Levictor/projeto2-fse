#include "../inc/menu.h"

void inicia_menu(){
  iniciox = 0;
  inicioy = 0;
  opcoes[ID_LAMP_1].chave = "LAMPADA 01";
  opcoes[ID_LAMP_1].valor = 0;
  opcoes[ID_LAMP_2].chave = "LAMPADA 01";
  opcoes[ID_LAMP_2].valor = 0;
  opcoes[ID_LAMP_CORR].chave = "LAMPADA CORR";
  opcoes[ID_LAMP_CORR].valor = 0;
  opcoes[ID_AR_C].chave = "AR CONDICIONADO";
  opcoes[ID_AR_C].valor = 0;
  opcoes[ID_ASP_ALR].chave = "ASPERSOR";
  opcoes[ID_ASP_ALR].valor = 0;
  opcoes[ID_SAIR].chave = "SAIR";
  opcoes[ID_SAIR].valor = 0;

  initscr();
  clear();
  noecho();
  cbreak();       /* Buffer de linha desativado. passa tudo */

  iniciox = (80 - WIDTH) / 2;
  inicioy = (24 - HEIGHT) / 2;

  cabecalho_win = newwin(HEIGHT - 5, WIDTH, 2, iniciox);

  menu_win = newwin(HEIGHT, WIDTH, inicioy + 2, iniciox);

  cabecalho_win_2 = newwin(HEIGHT - 5, WIDTH, 2, iniciox + 38);

  menu_win_2 = newwin(HEIGHT, WIDTH, inicioy + 2, iniciox + 38);

  keypad(menu_win_2, TRUE);
  keypad(menu_win, TRUE);
  mvprintw(0, 0, "Use as setas para subir e descer, pressione Enter para selecionar uma escolha");
  refresh();
  print_header(cabecalho_win_2);
  refresh();
  print_menu(menu_win_2, 0);
  refresh();
  print_header(cabecalho_win);
  refresh();
  print_menu(menu_win, 0);
}

void menu_interativo(){
  int escolha = -1;
  int destaque = 0;
  int c;
	int menu_disponivel = 1;
  WINDOW *menu_selecionado = menu_win;
  while( menu_disponivel ){
    c = wgetch(menu_selecionado);
    switch(c){
      case KEY_LEFT:
      case KEY_RIGHT:
        if(menu_selecionado == menu_win)
          menu_selecionado = menu_win_2;
        else
          menu_selecionado = menu_win;
        destaque = 0;
        print_menu(menu_selecionado, destaque);
        break;
      case 107:
      case KEY_UP:
        if(destaque == ID_LAMP_1)
          destaque = ID_SAIR;
        else
          --destaque;
        print_menu(menu_selecionado, destaque);
        break;

      case 106:
      case KEY_DOWN:
        if(destaque == ID_SAIR)
          destaque = 0;
        else
          ++destaque;
        print_menu(menu_selecionado, destaque);
        break;

      case 10:
        escolha = destaque;
        break;

      default:
        mvprintw(24, 0, "                                                                       ");
        refresh();
        mvprintw(24, 0, "O número do caractere que você digitou é = %3d e corresponde à: '%c'", c, c);
        refresh();
        break;
    }

    float valor;

    if(escolha == ID_LAMP_1){
      opcoes[ID_LAMP_1].valor = !opcoes[ID_LAMP_1].valor;
      print_menu(menu_selecionado, escolha);
      escolha = -1;
    }
    else if(escolha == ID_LAMP_2){
      opcoes[ID_LAMP_2].valor = !opcoes[ID_LAMP_2].valor;
      print_menu(menu_selecionado, destaque);
      escolha = -1;
    }
    else if( escolha > 0 && escolha != ID_SAIR && opcoes[ID_LAMP_1].valor){
      mvprintw(23, 0, "                            ");
      refresh();
      echo();
      mvscanw(23,0,"%f\n", &valor);
      noecho();
      refresh();
      switch(escolha){
        case ID_AR_C:
          // HISTERESE
          break;
        case ID_LAMP_CORR:
          // TR
          break;
        case ID_ASP_ALR:
          // KP
          break;
        case ID_SAIR:
          menu_disponivel = 0;
          break;
      }
      print_menu(menu_selecionado, escolha);
      escolha = -1;
    }

    else if(escolha == ID_SAIR)
      menu_disponivel = 0;
  }

  clrtoeol();
  refresh();
  endwin();
}

void print_header(WINDOW *cabecalho_win){
  int x, y;

  x = 2;
  y = 2;
  box(cabecalho_win, 0, 0);
  mvwprintw(cabecalho_win, y, x, "Andar: %s", cliente.andar == 'T' ? "Terreo" : "1");
  y++;
  mvwprintw(cabecalho_win, y, x, "Temperatura: %.02f", temp_int_men);
  y++;
  mvwprintw(cabecalho_win, y, x, "Humidade: %.02f", temp_ext_men);
  wrefresh(cabecalho_win);
}

void print_menu(WINDOW *menu_win, int destaque){
  int x, y, i;

  x = 2;
  y = 2;
  box(menu_win, 0, 0);
  for(i = 0; i < N_OPCOES; i++){
    if(destaque == i)
      wattron(menu_win, A_REVERSE);
      mvwprintw(menu_win, y, x, "%s - %d", &opcoes[i].chave[0], opcoes[i].valor);
    if(destaque == i)
      wattroff(menu_win, A_REVERSE);
    
    ++y;
  }
  wrefresh(menu_win);
}

