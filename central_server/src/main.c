#include<unistd.h>

#include "../inc/menu.h"
#include "../inc/global.h"
#include "../inc/servidor_tcp.h"

int main(){
  pthread_t tid;
	inicia_menu();
	write_headers();
	draw_menu();
	pthread_create(&tid, NULL, socket_server, NULL);

	return 0;
}
