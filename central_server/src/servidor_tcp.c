#include "../inc/servidor_tcp.h"

// TODO: Separar as varíaveis, não colocar juntas
// com isso fazer com que sejam enviadas uma de cada vez

//      system("aplay ../../utils/alarm.mp3");

void handle_sns_log(int *s, int n, char*sensor){
  char str[20];
  if(check_event(s, n)){
    sprintf(str,"%s %s", sensor, (*s == 1) ? "Acionado" : "Desativado");
    write_event(str);
  }
}

void *consulta_tcp(void *arg) {
  cliente_s *cliente = (cliente_s*) arg;
  int sock_cli = cliente->sock;
  mensagem msg;
  msg.sensor = -1;
  fd_set fds;
  struct timeval tv;
  tv.tv_sec = 3;
  int tamanho_mensagem;
  temp_hum buffer;
  while(1){
    select(sock_cli, &fds, NULL, NULL, &tv);
    send(sock_cli, &msg, 1, 0);
    tamanho_mensagem = recv(cliente->sock, &buffer, tamanho_mensagem, 0);
    if(tamanho_mensagem != sizeof(temp_hum))
      continue;
    if(cliente->andar == 0)
      set_temp_hum_t(buffer);
    else
      set_temp_hum_1(buffer);
    sleep(1);
  }
  return NULL;
}
void *receptor_tcp(void *arg) {
  cliente_s *cliente = (cliente_s*) arg;
  while(1){
    mensagem msg;
    recv(cliente->sock, &msg, sizeof(mensagem), 0);
    if(msg.sensor == -1 && msg.valor == -1)
      break;
    set_aparelho(msg.sensor, msg.valor);
  }
  return NULL;
}

void* socket_server() {
	int sock_serv;
	int sock_cli;
	SADDRIN end_serv;
	SADDRIN end_cli;
	unsigned int tam_cli;

	if((sock_serv = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		printf("falha no socker do Servidor\n");

	memset(&end_serv, 0, sizeof(end_serv));
	end_serv.sin_family = AF_INET;
	end_serv.sin_addr.s_addr = htonl(INADDR_ANY);
	end_serv.sin_port = htons(CENTRAL_PORT);

	if(bind(sock_serv, (SADDR *) &end_serv, sizeof(end_serv)) < 0)
		printf("Falha no Bind\n");

	if(listen(sock_serv, 10) < 0)
		printf("Falha no Listen\n");

  int andar_cliente;
	while(1) {
		tam_cli = sizeof(end_cli);
    sock_cli = accept(sock_serv, (SADDR *) &end_cli, &tam_cli);
    pthread_t tid_receptor, tid_consulta;

		if(sock_cli < 0)
      continue;

    recv(sock_cli, &andar_cliente, 1, 0);

    cliente.endereco = end_cli;
    cliente.sock = sock_cli;
    cliente.andar = andar_cliente;

		pthread_create(&tid_receptor, NULL, &receptor_tcp, (void*)&cliente);
		pthread_create(&tid_consulta, NULL, &consulta_tcp, (void*)&cliente);
    sleep(1);
	}

	close(sock_serv);
}

void envia_ordem(){
}
