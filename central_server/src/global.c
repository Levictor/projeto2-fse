#include "../inc/global.h"

temp_hum temp_hum_t;

temp_hum temp_hum_1;

pthread_t t_client,t_get_th, t_server;

aparelhos_terreo aparelhos_t;

aparelhos_andar aparelhos_a;

int controlados[6] = {0, 0, 0, 0, 0, 0};

void set_temp_hum_t(temp_hum arg){
  temp_hum_t.temperatura = arg.temperatura;
  temp_hum_t.humidade = arg.humidade;
}

void set_temp_hum_1(temp_hum arg){
  temp_hum_1.temperatura = arg.temperatura;
  temp_hum_1.humidade = arg.humidade;
}

void set_aparelho(int aparelho, int valor){
  switch(aparelho){
    case LS_T01:
      aparelhos_t.luz_1 = valor;
      break;
    case LS_T02:
      aparelhos_t.luz_2 = valor;
      break;
    case LC_T:
      aparelhos_t.luz_c = valor;
      break;
    case AC_T:
      aparelhos_t.ar = valor;
      break;
    case ASP:
      aparelhos_t.asp = valor;
      break;
    case LS_101:
      aparelhos_a.luz_1 = valor;
      break;
    case LS_102:
      aparelhos_a.luz_2 = valor;
      break;
    case LC_1:
      aparelhos_a.luz_c = valor;
      break;
    case AC_1:
      aparelhos_a.luz_c = valor;
      break;
    default:
      return ;
  }
}
