#include "../inc/cliente_tcp.h"

void close_socket(){
  close(clienteSocket);
}

void send_temp_hum(int msg){
  temp_hum buffer;

	if(send(clienteSocket, &msg, sizeof(int), 0) != sizeof(int))
		printf("Erro no envio: numero de bytes enviados diferente do esperado\n");

	int totalBytesRecebidos = 0;
	while(totalBytesRecebidos < sizeof(temp_hum)) {
		if((totalBytesRecebidos = recv(clienteSocket, &buffer, sizeof(temp_hum), 0)) <= 0)
			printf("Não recebeu o total de bytes enviados\n");
	}
  th.temp = buffer.temp;
  th.hum = buffer.hum;
}

void send_message(int msg){
  int buffer;
  msg--;
	if(send(clienteSocket, &msg, sizeof(int), 0) != sizeof(int))
		printf("Erro no envio: numero de bytes enviados diferente do esperado\n");

  if(msg > -10){
    int totalBytesRecebidos = 0;
    if(msg == -2){ // Get sensors initial state
      sensores sens;
      while(totalBytesRecebidos < sizeof(sensores)) {
    		if((totalBytesRecebidos = recv(clienteSocket, &sens, sizeof(sensores), 0)) <= 0)
    			printf("Não recebeu o total de bytes enviados\n");

  	   }
       set_sns(sens);
    }
    else if(msg == -3){ // Get Lamps/Airc initial state
      if((totalBytesRecebidos = recv(clienteSocket, &controlados, 6*sizeof(int), 0)) <= 0)
        printf("Não recebeu o total de bytes enviados\n");
    }

  	while(totalBytesRecebidos < sizeof(int)) {
  		if((totalBytesRecebidos = recv(clienteSocket, &buffer, sizeof(int), 0)) <= 0)
  			printf("Não recebeu o total de bytes enviados\n");

	   }

    handle_return(buffer);
  }
}

void init_client(){
	// Criar Socket
	if((clienteSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		printf("Erro no socket()\n");

	// Construir struct sockaddr_in
	memset(&servidorAddr, 0, sizeof(servidorAddr)); // Zerando a estrutura de dados
	servidorAddr.sin_family = AF_INET;
	servidorAddr.sin_addr.s_addr = inet_addr(SERVER_IP);
	servidorAddr.sin_port = htons(PORT);

	// Connect
	if(connect(clienteSocket, (struct sockaddr *) &servidorAddr, sizeof(servidorAddr)) < 0)
		printf("Erro no connect()\n");

}

int handle_return(int in){
  switch(in){
    case LAMP1_TURNOFF:
      controlados[0] = 0;
      write_event("LAMP1 Desligada");
    break;
    case LAMP1_TURNON:
      controlados[0] = 1;
      write_event("LAMP1 Ligada");
    break;
    case LAMP2_TURNOFF:
      controlados[1] = 0;
      write_event("LAMP2 Desligada");
    break;
    case LAMP2_TURNON:
      controlados[1] = 1;
      write_event("LAMP1 Ligada");
    break;
    case LAMP3_TURNOFF:
      controlados[2] = 0;
      write_event("LAMP3 Desligada");
    break;
    case LAMP3_TURNON:
      controlados[2] = 1;
      write_event("LAMP3 Ligada");
    break;
    case LAMP4_TURNOFF:
      controlados[3] = 0;
      write_event("LAMP4 Desligada");
    break;
    case LAMP4_TURNON:
      controlados[3] = 1;
      write_event("LAMP4 Ligada");
    break;
    case AIRC1_TURNOFF:
      controlados[4] = 0;
      write_event("AIRC1 Desligado");
    break;
    case AIRC1_TURNON:
      controlados[4] = 1;
      write_event("AIRC1 Ligado");
    break;
    case AIRC2_TURNOFF:
      controlados[5] = 0;
      write_event("AIRC2 Desligado");
    break;
    case AIRC2_TURNON:
      controlados[5] = 1;
      write_event("AIRC2 Ligado");
    break;
  }
  return in;
}
