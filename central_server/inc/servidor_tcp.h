#ifndef _SERVIDOR_TCP_H
#define _SERVIDOR_TCP_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/select.h>

#include "./global.h"
#include "menu.h"

int check_event(int*,int);
void handle_sns_log(int*,int,char*);
void *consulta_tcp(void *arg);
void *receptor_tcp(void *arg);
void *socket_server();
void receber_sensores_iniciais(cliente_s);
void envia_ordem();

#endif
