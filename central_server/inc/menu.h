#ifndef MENU_H
#define MENU_H

#include <ncurses.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "../inc/global.h"

#define WIDTH 37
#define HEIGHT 12
#define N_OPCOES 6
#define ID_LAMP_1 0
#define ID_LAMP_2 1
#define ID_LAMP_CORR 2
#define ID_AR_C 3
#define ID_ASP_ALR 4
#define ID_SAIR 5

typedef struct menu_struct{
  char *chave;
  int valor;
}menu_struct;

menu_struct opcoes[N_OPCOES];

float temp_ref_men, temp_ext_men, temp_int_men;

int iniciox;
int inicioy;

WINDOW *menu_win;
WINDOW *cabecalho_win;
WINDOW *menu_win_2;
WINDOW *cabecalho_win_2;

void print_menu(WINDOW *menu_win, int destaque);
void inicia_menu();
void print_header(WINDOW *);
void menu_interativo();
void inserir_informacoes(int, float, float, int);


#endif
