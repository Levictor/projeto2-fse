#ifndef _CLIENTE_TCP_H
#define _CLIENTE_TCP_H

#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../inc/global.h"
#include "../../utils/socket_constant.h"
#include "../inc/write_file.h"
#include "../inc/servidor_tcp.h"

#define PORT 10128
#define SERVER_IP "192.168.0.52"



int clienteSocket;
struct sockaddr_in servidorAddr;

void send_temp_hum(int);
void send_message(int);
void init_client();
void close_socket();

int handle_return(int);

#endif
