#ifndef _WRITE_FILE_H
#define _WRITE_FILE_H

#include <time.h>
#include <stdio.h>

#define FILE_NAME "log_file.csv"

void write_event(char*);
void write_headers();

#endif
