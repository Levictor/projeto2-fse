#ifndef _GLOBAL_H
#define _GLOBAL_H

#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "../../utils/socket_constant.h"

#define SADDR struct sockaddr
#define SADDRIN struct sockaddr_in


typedef struct cliente_s{
	SADDRIN endereco;
	int sock;
	int andar;
} cliente_s;


extern temp_hum temp_hum_t;

extern temp_hum temp_hum_1;

extern int row, col;

extern aparelhos_terreo aparelhos_t;

extern aparelhos_andar aparelhos_a;

extern cliente_s cliente;

void set_sensores_terreo(sensores_terreo);

void set_sensores_andar(sensores_andar);

void set_aparelho(int sensor, int valor);

void set_temp_hum_t(temp_hum);

void set_temp_hum_1(temp_hum);

#endif
